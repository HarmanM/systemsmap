/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/heatmaps";
exports.ids = ["pages/heatmaps"];
exports.modules = {

/***/ "./pages/heatmaps/index.tsx":
/*!**********************************!*\
  !*** ./pages/heatmaps/index.tsx ***!
  \**********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": function() { return /* binding */ Heatmaps; }\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_google_maps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-google-maps */ \"react-google-maps\");\n/* harmony import */ var react_google_maps__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_google_maps__WEBPACK_IMPORTED_MODULE_1__);\n\nvar _jsxFileName = \"/Users/harman/Documents/Helpseeker/Repos/systemsmap/pages/heatmaps/index.tsx\";\n\n\nfunction Map() {\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_google_maps__WEBPACK_IMPORTED_MODULE_1__.GoogleMap, {\n    defaultZoom: 4,\n    defaultCenter: {\n      lat: 56.130367,\n      lng: -106.346771\n    }\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 10,\n    columnNumber: 3\n  }, this);\n}\n\nconst Wrapper = (0,react_google_maps__WEBPACK_IMPORTED_MODULE_1__.withScriptjs)((0,react_google_maps__WEBPACK_IMPORTED_MODULE_1__.withGoogleMap)(Map));\nfunction Heatmaps() {\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n    style: {\n      width: \"100vw\",\n      height: \"100vh\"\n    },\n    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Wrapper, {\n      googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyCGS07GA30wZWdxK9eILpQNGb8LDz1P0Io`,\n      loadingElement: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        style: {\n          height: `100%`\n        }\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 25,\n        columnNumber: 25\n      }, this),\n      containerElement: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        style: {\n          height: `100%`\n        }\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 26,\n        columnNumber: 27\n      }, this),\n      mapElement: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        style: {\n          height: `100%`\n        }\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 27,\n        columnNumber: 21\n      }, this)\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 22,\n      columnNumber: 7\n    }, this)\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 21,\n    columnNumber: 5\n  }, this);\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zeXN0ZW1zbWFwLy4vcGFnZXMvaGVhdG1hcHMvaW5kZXgudHN4PzkxZTEiXSwibmFtZXMiOlsiTWFwIiwibGF0IiwibG5nIiwiV3JhcHBlciIsIndpdGhTY3JpcHRqcyIsIndpdGhHb29nbGVNYXAiLCJIZWF0bWFwcyIsIndpZHRoIiwiaGVpZ2h0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBR0E7O0FBSUEsU0FBU0EsR0FBVCxHQUFjO0FBQ1osc0JBQ0EsOERBQUMsd0RBQUQ7QUFDRSxlQUFXLEVBQUUsQ0FEZjtBQUVFLGlCQUFhLEVBQUU7QUFBQ0MsU0FBRyxFQUFFLFNBQU47QUFBaUJDLFNBQUcsRUFBRSxDQUFDO0FBQXZCO0FBRmpCO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFEQTtBQU1EOztBQUVELE1BQU1DLE9BQU8sR0FBR0MsK0RBQVksQ0FBQ0MsZ0VBQWEsQ0FBQ0wsR0FBRCxDQUFkLENBQTVCO0FBRWUsU0FBU00sUUFBVCxHQUFvQjtBQUMvQixzQkFDQTtBQUFNLFNBQUssRUFBRTtBQUFFQyxXQUFLLEVBQUUsT0FBVDtBQUFrQkMsWUFBTSxFQUFFO0FBQTFCLEtBQWI7QUFBQSwyQkFDRSw4REFBQyxPQUFEO0FBQ0Usa0JBQVksRUFDViwrSEFGSjtBQUdFLG9CQUFjLGVBQUU7QUFBSyxhQUFLLEVBQUU7QUFBRUEsZ0JBQU0sRUFBRztBQUFYO0FBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUhsQjtBQUlFLHNCQUFnQixlQUFFO0FBQUssYUFBSyxFQUFFO0FBQUVBLGdCQUFNLEVBQUc7QUFBWDtBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FKcEI7QUFLRSxnQkFBVSxlQUFFO0FBQUssYUFBSyxFQUFFO0FBQUVBLGdCQUFNLEVBQUc7QUFBWDtBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURBO0FBV0QiLCJmaWxlIjoiLi9wYWdlcy9oZWF0bWFwcy9pbmRleC50c3guanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgSGVhZCBmcm9tICduZXh0L2hlYWQnXG5pbXBvcnQgSW1hZ2UgZnJvbSAnbmV4dC9pbWFnZSdcbmltcG9ydCBzdHlsZXMgZnJvbSAnLi4vc3R5bGVzL0hvbWUubW9kdWxlLmNzcydcbmltcG9ydCB7R29vZ2xlTWFwLFxuICAgICAgICB3aXRoR29vZ2xlTWFwLFxuICAgICAgICB3aXRoU2NyaXB0anN9IGZyb20gXCJyZWFjdC1nb29nbGUtbWFwc1wiXG5cbmZ1bmN0aW9uIE1hcCgpe1xuICByZXR1cm4gKFxuICA8R29vZ2xlTWFwIFxuICAgIGRlZmF1bHRab29tPXs0fSAgXG4gICAgZGVmYXVsdENlbnRlcj17e2xhdDogNTYuMTMwMzY3LCBsbmc6IC0xMDYuMzQ2NzcxfX0gXG4gICAgLz5cbiAgKTtcbn1cblxuY29uc3QgV3JhcHBlciA9IHdpdGhTY3JpcHRqcyh3aXRoR29vZ2xlTWFwKE1hcCkpO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIZWF0bWFwcygpIHtcbiAgICByZXR1cm4gKFxuICAgIDxkaXYgIHN0eWxlPXt7IHdpZHRoOiBcIjEwMHZ3XCIsIGhlaWdodDogXCIxMDB2aFwiIH19PlxuICAgICAgPFdyYXBwZXJcbiAgICAgICAgZ29vZ2xlTWFwVVJMPVxuICAgICAgICB7YGh0dHBzOi8vbWFwcy5nb29nbGVhcGlzLmNvbS9tYXBzL2FwaS9qcz92PTMuZXhwJmxpYnJhcmllcz1nZW9tZXRyeSxkcmF3aW5nLHBsYWNlcyZrZXk9QUl6YVN5Q0dTMDdHQTMwd1pXZHhLOWVJTHBRTkdiOExEejFQMElvYH0gIFxuICAgICAgICBsb2FkaW5nRWxlbWVudD17PGRpdiBzdHlsZT17eyBoZWlnaHQ6IGAxMDAlYCB9fSAvPn1cbiAgICAgICAgY29udGFpbmVyRWxlbWVudD17PGRpdiBzdHlsZT17eyBoZWlnaHQ6IGAxMDAlYCB9fSAvPn1cbiAgICAgICAgbWFwRWxlbWVudD17PGRpdiBzdHlsZT17eyBoZWlnaHQ6IGAxMDAlYCB9fSAvPn1cbiAgICAgIC8+XG4gICAgPC9kaXY+XG4gICAgKVxuICB9Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/heatmaps/index.tsx\n");

/***/ }),

/***/ "react-google-maps":
/*!************************************!*\
  !*** external "react-google-maps" ***!
  \************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-google-maps");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/heatmaps/index.tsx"));
module.exports = __webpack_exports__;

})();