import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import {GoogleMap,
        withGoogleMap,
        withScriptjs} from "react-google-maps"

function Map(){
  return (
  <GoogleMap 
    defaultZoom={4}  
    defaultCenter={{lat: 56.130367, lng: -106.346771}} 
    />
  );
}

const Wrapper = withScriptjs(withGoogleMap(Map));

export default function Heatmaps() {
    return (
    <div  style={{ width: "100vw", height: "100vh" }}>
      <Wrapper
        googleMapURL=
        {`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyCGS07GA30wZWdxK9eILpQNGb8LDz1P0Io`}  
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    </div>
    )
  }